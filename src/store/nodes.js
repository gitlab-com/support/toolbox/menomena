import flux from "@aust/react-flux";

function initialSettings() {
  return {};
}

const store = flux.addStore("params", initialSettings());

// ========================================================================
// -- Replace
// ========================================================================
store.register("nodes/replace", async (_, kind, obj) => {
  return (state) => ({
    ...state,
    [kind]: { ...state[kind], [obj.id]: obj },
  });
});

// ========================================================================
// -- Remove
// ========================================================================
store.register("nodes/remove", async (_, kind, objId) => (state) => {
  const { [objId]: removed, ...kindData } = state[kind];
  return {
    ...state,
    [kind]: kindData,
  };
});

// ========================================================================
// -- Show
// ========================================================================
store.addSelector("nodes/show", (state, kind, objId) => {
  return state[kind][objId];
});
