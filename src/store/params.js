import flux from "@aust/react-flux";

function initialSettings() {
  return {};
}

const store = flux.addStore("params", initialSettings());

// Entry point
store.register("params/init", async (dispatch) => {
  await flux.dispatch("params/readQuery");

  // Current is the only required param
  let current = store.selectState("current");
  if (current) {
    dispatch("sys/nav", "path");
  } else {
    dispatch("sys/nav", "start");
  }
});

// ========================================================================
// -- Store Updates
// ========================================================================
store.register("params/update", async (dispatch, payload) => {
  return (state) => ({ ...state, ...payload });
});
// ========================================================================

// ========================================================================
// -- Query Update
// ========================================================================
store.register("params/setQuery", async () => {
  // let params = store.selectState();

  // Construct URLSearchParams object instance from current URL querystring.
  var queryParams = new URLSearchParams();

  // if (params.current) queryParams.set("current", params.current.version);
  // if (params.target && VersionList.targets[0] !== params.target)
  //   queryParams.set("target", params.target.version);

  // // Ignore Defaults
  // if (params.distro !== "ubuntu") queryParams.set("distro", params.distro);
  // if (params.auto) queryParams.set("auto", params.auto);
  // if (params.edition !== "ee") queryParams.set("edition", params.edition);
  // if (params.downtime) queryParams.set("downtime", params.downtime);

  // Set new or modify existing parameter value.

  // Replace current querystring with the new one.
  window.history.replaceState(null, null, "?" + queryParams.toString());
});
// ========================================================================
