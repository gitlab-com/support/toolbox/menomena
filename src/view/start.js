import React from "react";
import { Button, Container, Typography } from "@mui/material";
import Div from "util/div";
import Section from "components/section";

function Start() {
  return (
    <Div direction='column'>
      <Div {...style.header}>
        <Typography variant='h4'>Mahna Mahna</Typography>
      </Div>

      <Div {...style.btns}>
        <Button sx={{ margin: 1 }} variant='contained'>
          Add Node
        </Button>
        <Button sx={{ margin: 1 }} variant='contained' color='success'>
          Add Section
        </Button>
      </Div>

      <Div {...style.container}>
        <Container>
          <Section nodes={loadBalancer} />
          <Section title='Rails' nodes={railsNodes} />
          <Section title='Data' nodes={databaseNodes} />
          <Section title='Storage' nodes={storageNodes} />
          <Section title='Monitoring' nodes={monitoringNodes} />
        </Container>
      </Div>
    </Div>
  );
}

const loadBalancer = [
  {
    id: "Loadbalancer",
    name: "Loadbalancer",
    description: "ALB",
    tags: [{ id: "ssl", label: "Terminated SSL" }],
  },
];

const railsNodes = [
  {
    id: "rails1",
    name: "Rails 1",
    color: "green",
    tags: [{ id: "sidekiq", label: "sidekiq" }],
  },
  {
    id: "rails2",
    name: "Rails 2",
    color: "green",
    tags: [{ id: "deploy node", label: "Deploy Node", color: "error" }],
  },
  { id: "rails3", color: "green", name: "Rails 3" },
];

const monitoringNodes = [
  { id: "prom", color: "grey", name: "Prometheus" },
  { id: "graf", color: "grey", name: "Grafana" },
];

const databaseNodes = [
  {
    id: "pgs",
    name: "PostgreSQL",
    description: "RDS",
    color: "purple",
    tags: [
      {
        id: "version",
        label: "12.10",
      },
    ],
  },
  {
    id: "redis",
    name: "Redis",
    color: "red",
    description: "ElastiCache",
    tags: [
      {
        id: "version",
        label: "6.0",
      },
    ],
  },
];
const storageNodes = [
  {
    id: "gitaly",
    name: "Gitaly",
    color: "darkorange",
    description: "Standalone",
  },
  {
    id: "object",
    name: "Object Storage",
    description: "AWS S3",
    tags: [
      { id: "artifacts", label: "artifacts", color: "success" },
      { id: "artifacts", label: "uploads", color: "secondary" },
      { id: "artifacts", label: "packages" },
    ],
  },
];

export default Start;

const style = {
  header: {
    flex: 0,
    justify: "center",
  },

  btns: {
    flex: 0,
    justify: "center",
  },

  container: {
    flex: 1,
    direction: "column",
  },
};
