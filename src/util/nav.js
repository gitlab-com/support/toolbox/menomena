import React from "react";
import flux from "@aust/react-flux";
import { createTheme, ThemeProvider } from "@mui/material/styles";

// Views
import Start from "view/start";

// Theme
const darkTheme = createTheme({
  palette: {
    mode: "dark",
  },
});

const Nav = () => {
  const status = flux.sys.useState("status");

  return (
    <div style={style.view}>
      <ThemeProvider theme={darkTheme}>
        <div style={{ position: "absolute", left: 0, bottom: 0 }}>
          Path:{status}{" "}
        </div>

        {status === "start" && <Start />}
      </ThemeProvider>
    </div>
  );
};

export default Nav;

const style = {
  view: {
    display: "flex",
    flex: 1,
    height: "100vh",
    width: "100vw",
    flexDirection: "column",
    overflow: "hidden",
  },
};
