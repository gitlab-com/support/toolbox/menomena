import React from "react";
import Div from "util/div";
import Node from "./node";

function Section({
  title = "External Load Balancer",
  nodes = [{ name: "ALB", id: "rawrawr" }],
}) {
  return (
    <Div {...style.view}>
      <Div {...style.title}>{title}</Div>
      <Div>
        {nodes.map((x) => (
          <Node key={x.id} {...x} />
        ))}
      </Div>
    </Div>
  );
}

export default Section;

const style = {
  view: {
    flex: 0,
    direction: "column",
    style: {
      backgroundColor: "rgba(0,0,0,0.4)",
      padding: "1rem",
      margin: "0.4rem",
    },
  },

  title: {
    justify: "center",
    style: {
      marginBottom: "1rem",
      fontSize: "1.5rem",
      borderBottom: "solid 1px rgba(255,255,255,0.4)",
    },
  },
};
