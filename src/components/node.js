import React from "react";
// import { Button } from "@mui/material";

// Local
import Div from "util/div";
import Tags from "./tags";

function Node({
  name = "ALB",
  description = null,
  tags = null,
  color = "dodgerblue",
}) {
  return (
    <Div {...style.view(color)}>
      <Div {...style.title}>{name}</Div>
      {description && <Div {...style.desc}>{description}</Div>}

      {tags && <Tags list={tags} />}

      {/* <Div>
        <Button>Config</Button>
        <Button>Help</Button>
      </Div> */}
    </Div>
  );
}

export default Node;

const style = {
  view: function (color = "dodgerblue") {
    return {
      flex: 1,
      direction: "column",
      justify: "center",
      align: "center",
      style: {
        backgroundColor: color,
        border: "solid 1px black",
        borderRadius: "1rem",
        padding: "2rem",
        margin: "0.4rem",
      },
    };
  },

  title: {
    justify: "center",
    align: "center",
    style: {
      fontSize: "1.5rem",
      fontWeight: "strong",
    },
  },

  desc: {
    justify: "center",
    style: {
      // fontSize: "1.5rem",
    },
  },
};
