import React from "react";
import Chip from "@mui/material/Chip";

// Local
import Div from "util/div";

const DefaultChip = ({
  label = "Default",
  size = "small",
  variant = "contained",
  color = "primary",
  sx = { margin: 0.4 },
}) => {
  return (
    <Chip sx={sx} label={label} size={size} variant={variant} color={color} />
  );
};

function Tags({ list }) {
  return (
    <Div>
      {list.map((x) => (
        <DefaultChip key={x.id} {...x} />
      ))}
    </Div>
  );
}

export default Tags;
